# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

(((λp.pz) λq.w) λw.wqzp)

2. λp.pq λp.qp

((λp.pq) λp.qp)

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

s bound to λs and q is bound to λq, z is not bound and s is not bound

2. (λs. s z) λq. w λw. w q z s

s and w bound are bound on λs and λw respectively. The rest are free

3. (λs.s) (λq.qs)

s bound to λs and q bound to λq, s not bound to λq

4. λz. (((λs.sq) (λq.qz)) λz. (z z))

s bound to λs, q bound to λq, z bound to λz; s and q unbound

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)

(z) [z -> λq.q q] (λs.s a)

(λq.q q) (λs.s a)

(q q)[q -> (λs.s a)]

(λs.s a) (λs.s a)

(s a) [s -> (λs.s a)]

(λs.s a) a {change a -> b}

(s a) [s -> b]

b a

2. (λz.z) (λz.z z) (λz.z q)

(z) [z -> (λz.z z)] (λz.z q)

(λz.z z) (λz.z q)

(z z) [z -> (λz.z q)]

(λz.z q) (λz.z q)

(λz.z q) (λa.a b)

(z q) [z -> (λa.a b)]

(λa.a b) q

(a b) [a -> q]

q b

3. (λs.λq.s q q) (λa.a) b

(λq.s q q) [s -> (λa.a)] b

(λq.(λa.a) q q) b

((λa.a) q q) [q -> b]

(λa.a) b b

(a) [a -> b] b

b b

4. (λs.λq.s q q) (λq.q) q

(λs.λq.s q q) (λa.a) q

(λq.s q q) [s -> (λa.a)] q

(λq.(λa.a) q q) q

((λa.a) q q) [q -> q]

(λa.a) q q

(a) [a -> q] q

q q

5. ((λs.s s) (λq.q)) (λq.q)

(s s) [s -> (λq.q)]  (λq.q)

((λq.q) (λq.q)) (λq.q)

(q) [q -> (λq.q)] (λq.q)

(λq.q) (λq.q)

(q) [q -> (λq.q)]

(λq.q)

## Question 4

1. Write the truth table for the or operator below.

F ≡ λxy.y
T ≡ λxy.x

TF -> T
FT -> T
TT -> T
FF -> F

∨ ≡ λxy.x(λuv.u)y ≡ λxy.xTy

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

λxy.xTy -> x = T, y = T

(λxy.x) (λxy.x) (λxy.x)
(λy.x) [x -> λxy.x] (λxy.x)
(λy.(λxy.x))(λxy.x)
(λxy.x) [y -> λxy.x]
λxy.x ------> T

λxy.xTy -> x = F, y = T

(λxy.y) (λxy.x) (λxy.x)
(λy.y) [x -> λxy.x] (λxy.x)
(λy.y)(λxy.x)
(y) [y -> λxy.x]
λxy.x ------> T

λxy.xTy -> x = T, y = F

(λxy.x) (λxy.x) (λxy.y)
(λy.x) [x -> (λxy.x)] (λxy.y)
(λy.(λxy.x))(λxy.y)
(λxy.x) [y -> λxy.y]
λxy.x ------> T

λxy.xTy -> x = F, y = F

(λxy.y) (λxy.x) (λxy.y)
(λy.y) [x -> λxy.x] (λxy.y)
(λy.y) (λxy.y)
(y) [y -> (λxy.y)]
λxy.y ------> F

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

¬ ≡ λx.x(λuv.v)(λab.a) ≡ λx.xFT

F ≡ λxy.y
T ≡ λxy.x

T -> F
F -> T

λx.xFT -> x = F

(λx.x(λuv.v)(λab.a)) (λxy.y)
(x(λuv.v) (λab.a)) [x -> (λxy.y)]
(λxy.y) (λuv.v) (λab.a)
(λy.y) [x -> (λuv.v)] (λab.a)
(λy.y) (λab.a)
(y) [y -> (λab.a)]
λab.a ------> T

λx.xFT -> x = T

(λx.x(λuv.v)(λab.a)) (λxy.x)
(x(λuv.v) (λab.a)) [x -> (λxy.x)]
(λxy.x) (λuv.v) (λab.a)
(λy.x) [x -> (λuv.v)] (λab.a)
(λy.(λuv.v)) (λab.a)
(λuv.v) [y -> (λab.a)]
λuv.v ------> F

## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.